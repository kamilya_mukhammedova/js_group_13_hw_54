import { Component } from '@angular/core';
import {Cell} from "../cell/cellClass";

@Component({
  selector: 'app-playing-field',
  templateUrl: './playing-field.component.html',
  styleUrls: ['./playing-field.component.css']
})
export class PlayingFieldComponent  {
  cellArray: Cell[] = [];
  userTries = 0;
  messageToUser = false;

  constructor() {
    for(let i = 0; i < 36; i++) {
      this.cellArray.push(new Cell());
    }
    this.cellArray[Math.floor(Math.random() * this.cellArray.length)].hasItem = true;
  }

  changeClassName(index: number) {
    const className = this.cellArray[index].isClicked ? 'white' : 'grey';
    return 'cell ' + className;
  }

  changeCell(index: number) {
    this.cellArray[index].isClicked = !this.cellArray[index].isClicked;
    if(this.cellArray[index].isClicked) {
      this.userTries++;
    }
    if(this.cellArray[index].hasItem) {
      this.messageToUser = !this.messageToUser;
    }
  }

  getReset() {
    this.cellArray = [];
    this.userTries = 0;
    this.messageToUser = false;
    for(let i = 0; i < 36; i++) {
      this.cellArray.push(new Cell());
    }
    this.cellArray[Math.floor(Math.random() * this.cellArray.length)].hasItem = true;
  }
}
