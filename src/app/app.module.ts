import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PlayingFieldComponent } from './playing-field/playing-field.component';
import { CellComponent } from './cell/cell.component';
import { ControlBlockComponent } from './control-block/control-block.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayingFieldComponent,
    CellComponent,
    ControlBlockComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
