import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-control-block',
  templateUrl: './control-block.component.html',
  styleUrls: ['./control-block.component.css']
})
export class ControlBlockComponent {
 @Input() tries = 0;
 @Output() buttonReset = new EventEmitter();
 @Input() showedMessage = false;
 message = 'Success! You have found the secret element!';

  reset() {
    this.buttonReset.emit();
  }
}
