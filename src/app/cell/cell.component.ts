import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
 @Input() gettingClass = '';
 @Output() clickedCell = new EventEmitter();
 @Input() gettingSymbol = false;
 symbol = 'o';

  getClickCell() {
    this.clickedCell.emit();
  }
}
